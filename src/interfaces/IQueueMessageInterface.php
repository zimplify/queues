<?php
    namespace Zimplify\Queues\Interfaces;

    /**
     * this interface provide some basic details that we need to endorse for each queue message
     * 
     */
    interface IQueueMessageInterface {

        const FLD_BODY = "body";
        const FLD_FROM = "from";
        const FLD_SENT = "sent";        
        const FLD_TASK = "type";
        
    }