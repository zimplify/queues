<?php
    namespace Zimplify\Queues\Interfaces;
    use Zimplify\Core\Request;

    /**
     * this interface provide the basic core definitions to all queue enabled servies to follow
     * 
     */
    interface IQueueServiceInterface {

        /**
         * command to initiate the client to the queue
         * @return void
         */
        function connect() : void;

        /**
         * dispatch a request to the queue
         * @param Request $request the request to issue to the queue
         * @return string
         */
        function dispatch(Request $request) : string;

        /**
         * check if the queue is ready for service
         * @return bool
         */
        function isConnected() : bool;        

        /**
         * collect request from queue and for further processing
         * @param bool $clean whether the message must be cleaned after pickup
         * @return Request
         */
        function pickup(bool $clean = true) : Request;
    }