<?php
    namespace Zimplify\Queues\Providers;
    use Zimplify\Core\{Application, Provider, Request};
    use Zimplify\Core\Services\ClassUtils;

    /**
     * this provider offer a platform independent entry point for the sending and receiving message from queue
     * @package Zimplify\Queue (code 06)
     * @type provider (code 03)
     * @file QueueProvider (code 01)
     */
    class QueueProvider extends Provider {

        const ARGS_QUEUE_NAME = "queue";
        const CFG_SYSTEM_QUEUE = "system.queue.setup";
        const ERR_BAD_CONFIG = 500060301001;
        const ERR_BAD_ENGINE = 500060301002;
        const ERR_INCOMPATIBLE = 500060301003;
        const FLD_AGENT = "engine";
        const FLD_CREDENTIAL = "credential";

        private $client;
        private $setup = [];

        /**
         * validate if our driver data is valid
         * @param array $driver the incoming driver configuration
         * @return bool
         */
        protected function cleanse(array $driver) : bool {
            $result = false;
            if (array_key_exists(self::FLD_AGENT, $driver) && array_key_exists(self::FLD_CREDENTIAL, $setup))  
                $result = true;
            return $result;
        }

        /**
         * engaging our target engine
         * @return mixed
         */
        private function connect() {
            $target = $setup[self::FLD_AGENT];
            $adapter = Application::request($target, $setup[self::FLD_CREDENTIAL]);
            if ($adapter) {
                if (ClassUtils::isImplemented($adapter, "IQueueServiceInterface")) {
                    $this->client = $adapter;
                    return $this->client;    
                } else 
                    throw new RuntimeException("Adapter is not cater for queue services.", self::ERR_INCOMPATIBLE);
            } else 
                throw new RuntimeException("Unable to initialize engine $target.", self::ERR_BAD_ENGINE);
        }

        /**
         * startup initializer for the service
         * @return void
         */
        protected function initialize() {
            if ($this->cleanse($driver = Application::env(self::CFG_SYSTEM_QUEUE) ?? [])) 
                $this->setup = $driver;
            else 
                throw new RuntimeException("Driver configuration is not complete.", self::ERR_BAD_CONFIG);
        }

        /**
         * check if all startup arguments are available
         * @return bool
         */
        protected function isRequired() : bool {
            return !is_null($this->get(self::ARGS_QUEUE_NAME));
        }

        /**
         * need to track the receiving of message
         * @param bool $clean a flag to indicate whether we need to remove the message after reading. default is to yes (true)
         * @return Request
         */
        public function receive(bool $clean = true) : Request {

            // make sure our connection is ready
            if (!$this->client->isConnected()) $this->connect();

            // now read the message
            $data = $this->client->pickup($clean);
            
            // now digest the message
            $result = new Request();
            $data = json_decode($data, true);
            foreach ($data as $field => $value) {
                $result->{$field} = $value;                
            }

            // now return 
            return $result;
        }
        
        /**
         * sending out a message to the queue
         * @param Request $request the request to send out to queue
         * @return string
         */
        public function send(Request $request) : string {

            // make sure our queue is really ready
            if (!$this->client->isConnected()) $this->connect();

            // do the dispatch
            $result = $this->client->dispatch($request);

            // return the result
            return $result;
        }

    }